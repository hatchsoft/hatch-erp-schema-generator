ALTER TABLE [dbo].[PermissionGroupItem] DROP CONSTRAINT [FK_PermissionGroupItem_Permission];
ALTER TABLE [dbo].[PermissionGroupItem] DROP CONSTRAINT [FK_PermissionGroupItem_PermissionGroup];
ALTER TABLE [dbo].[UserPermission] DROP CONSTRAINT [FK_UserPermission_Permission];
ALTER TABLE [dbo].[UserPermission] DROP CONSTRAINT [FK_UserPermission_User];
ALTER TABLE [dbo].[UserPermissionGroup] DROP CONSTRAINT [FK_UserPermissionGroup_PermissionGroup];
ALTER TABLE [dbo].[UserPermissionGroup] DROP CONSTRAINT [FK_UserPermissionGroup_User];
ALTER TABLE [dbo].[UserRole] DROP CONSTRAINT [FK_UserRole_User];
ALTER TABLE [dbo].[UserRole] DROP CONSTRAINT [FK_UserRole_Role];
ALTER TABLE [dbo].[RolePermission] DROP CONSTRAINT [FK_RolePermission_Permission];
ALTER TABLE [dbo].[RolePermission] DROP CONSTRAINT [FK_RolePermission_Role];
ALTER TABLE [dbo].[RolePermissionGroup] DROP CONSTRAINT [FK_RolePermissionGroup_Role];
ALTER TABLE [dbo].[RolePermissionGroup] DROP CONSTRAINT [FK_RolePermissionGroup_PermissionGroup];

ALTER TABLE [dbo].[PermissionGroupItem] ADD CONSTRAINT [FK_PermissionGroupItem_Permission] FOREIGN KEY ([PermissionId]) REFERENCES [dbo].[Permission]([PermissionId]) 
ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE [dbo].[PermissionGroupItem] ADD CONSTRAINT [FK_PermissionGroupItem_PermissionGroup] FOREIGN KEY ([PermissionGroupId]) REFERENCES [dbo].[PermissionGroup]([PermissionGroupId]) 
ON DELETE NO ACTION ON UPDATE CASCADE;  
ALTER TABLE [dbo].[UserPermission] ADD CONSTRAINT [FK_UserPermission_Permission] FOREIGN KEY ([PermissionId]) REFERENCES [dbo].[Permission]([PermissionId]) 
ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE [dbo].[UserPermission] ADD CONSTRAINT [FK_UserPermission_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User]([UserId]) 
ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE [dbo].[UserPermissionGroup] ADD CONSTRAINT [FK_UserPermissionGroup_PermissionGroup] FOREIGN KEY ([PermissionGroupId]) REFERENCES [dbo].[PermissionGroup]([PermissionGroupId]) 
ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE [dbo].[UserPermissionGroup] ADD CONSTRAINT [FK_UserPermissionGroup_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User]([UserId]) 
ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE [dbo].[UserRole] ADD CONSTRAINT [FK_UserRole_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User]([UserId]) 
ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE [dbo].[UserRole] ADD CONSTRAINT [FK_UserRole_Role] FOREIGN KEY ([RoleID]) REFERENCES [dbo].[Role]([RoleId]) 
ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE [dbo].[RolePermission] ADD CONSTRAINT [FK_RolePermission_Permission] FOREIGN KEY ([PermissionId]) REFERENCES [dbo].[Permission]([PermissionId]) 
ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE [dbo].[RolePermission] ADD CONSTRAINT [FK_RolePermission_Role] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[Role]([RoleId]) 
ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE [dbo].[RolePermissionGroup] ADD CONSTRAINT [FK_RolePermissionGroup_Role] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[Role]([RoleId]) 
ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE [dbo].[RolePermissionGroup] ADD CONSTRAINT [FK_RolePermissionGroup_PermissionGroup] FOREIGN KEY ([PermissionGroupId]) REFERENCES [dbo].[PermissionGroup]([PermissionGroupId]) 
ON DELETE NO ACTION ON UPDATE CASCADE;