'use strict';
var _ = require('lodash');

var db2sequelize = require('./main');
var config = require('./config/config');
var modules = require('./config/modules');
var interfaces = require('./config/interfaces');

var resources = flattenModulesToResources(modules);

db2sequelize(config, resources, interfaces, config.filePath, function (err) {
  if (err) {
    console.log(err);
  } else {
    console.log('done.');
  }
});

function flattenModulesToResources(modules) {
  var resources = {};
  _.keys(modules).map(moduleName => {
    var module = modules[moduleName];
    _.keys(module).map(entityName => {
      if (entityName === 'module') {
        entityName = `${moduleName}.${entityName}`;
      }
      if (resources[entityName] != null) {
        throw new Error('Resource name must be unique!');
      }
      resources[entityName] = _.assign({}, module[entityName], {
        moduleName: moduleName,
        entityName: entityName,
        resourceKey: entityName.endsWith('module') ? entityName : `${moduleName}.${entityName}`,
      });
    });
  });
  return resources;
}