/**
 * A smart printer that auto indent JS file based on bracket
 */
var _ = require('lodash');

module.exports = function PrinterFactory(options) {
  var indent = options.indent;
  var TABSIZE = options.tabsize;
  var output = [];

  return {
    tab() { indent += TABSIZE },
    untab() { indent = indent < TABSIZE ? 0 : indent - TABSIZE },
    print(content) {
      content = content.trim();
      // auto indent by detecting brackets. Not fully tested.
      // doesn't work for one liner I guess.
      if (content.startsWith('}')) this.untab();
      output.push(content === '' ? content : _.repeat(' ', indent) + content);
      if (content.endsWith('{')) this.tab();
    },
    printAll() {
      return output.join('\n');
    },
    /**
     * Wrap a block of print, auto create blank line above if no content before
     */
    createBlock() {
      var counter = 0;
      return {
        // the `this` here refers to Printer
        start: () => { if (counter !== 0) this.print(''); },
        end() { counter++ },
        // the `this` here referes to block
        print(func) {
          this.start();
          func();
          this.end();
        },
      };
    },
  };
};