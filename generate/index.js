var GeneratorFactory = require('./GeneratorFactory');

module.exports = schema => {
  var generator = GeneratorFactory(schema);
  return generator.generate();
}