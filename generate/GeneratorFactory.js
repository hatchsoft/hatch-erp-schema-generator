var _ = require('lodash');
var inflect = require('inflect');
var PrinterFactory = require('./PrinterFactory');
var I = require('../config/interfaces');

/**
 * Generate a Standard Hatch ERP Sequelize def file
 * based on a entity schema
 * @param {*} schema
 */
module.exports = function GeneratorFactory(schema) {
  var p = PrinterFactory({ indent: 0, tabsize: 2 });
  var extraUniqueKeys = [];
  return {
    generate() {
      this.file(p, schema);
      return p.printAll();
    },
    file(p, schema) {
      p.print('module.exports = function (define, DataTypes, models) {');
      p.print('define({');
      p.print(`name: '${schema.tableName}',`);
      this.columns(p, schema);
      this.options(p, schema);
      this.extend(p, schema);
      p.print('});');
      p.print('};');
    },
    // main
    columns(p, schema) {
      p.print('columns: {');
      this.standardColumns(p, schema);
      this.interfaceColumns(p, schema);
      this.systemColumns(p, schema);
      p.print('},');
    },
    options(p, schema) {
      p.print('options: {');
      this.standardOptions(p, schema);
      p.print('},');
    },
    extend(p, schema) {
      p.print('extend: function (Model) {');
      this.standardExtend(p, schema);
      p.print('},');
    },
    // columns
    standardColumns(p, schema) {
      _.keys(schema.columns).map(columnName => {
        var attrs = schema.columns[columnName].attributes;

        p.print(`${columnName}: {`);
        p.print(`type: ${attrs.dataType},`);

        if (attrs.isJSON) {
          p.print('get: function () {')
          p.print(`const value = this.getDataValue('${columnName}');`)
          p.print('return value == null ? value : JSON.parse(value);')
          p.print('},')
          p.print(`set: function (val) { this.setDataValue('${columnName}', JSON.stringify(val)) },`)
        }

        if (attrs.dataType === 'DataTypes.UUID' && schema.pk.hasOwnProperty(columnName)) {
          p.print('defaultValue: DataTypes.UUIDV4,');
        }

        if (attrs.isIdentity) {
          p.print('autoIncrement: true,');
        }

        if (attrs.defaultValue != null) {
          p.print(`defaultValue: ${attrs.defaultValue},`);
        } else {
          // auto set default value as true for IsActive column if not specified
          if (columnName === 'isActive') {
            p.print('defaultValue: true,');
          }
        }

        if (schema.pk.hasOwnProperty(columnName)) {
          p.print('primaryKey: true,');
        }
        let hasUniqueKey = false;
        _.map(schema.uk[schema.tableName], function (ukColumns, key) {
          if (ukColumns.indexOf(columnName) !== -1) {
            key = key.split('_').map(_.camelCase).join('_');
            // push extra unique keys into `options`
            if (hasUniqueKey) {
              extraUniqueKeys.push({ ukColumns, key });
              ukColumns.skip = true;
              return;
            }
            if (ukColumns.skip) return;
            hasUniqueKey = true;

            // if unique key name is not important for single column unique key
            // if (ukColumns.length === 1)
            // 	p.print('      unique: true,');
            // else
            p.print('unique: \'' + key + '\',');
          }
        });

        if (!attrs.isNullable) {
          p.print(`allowNull: ${attrs.isNullable},`);
        }

        //p.print(i !== schema.columns.length - 1 ? '},' : '}');
        p.print('},');
      })
    },
    interfaceColumns(p, schema) {

    },
    systemColumns(p, schema) {
      p.print('tenantId: {');
      p.print('type: DataTypes.STRING(50),');
      p.print('allowNull: false,');

      // merge single unique to TenantId
      if (schema.uk[schema.tableName] && _.keys(schema.uk[schema.tableName]).length === 1) {
        let key = _.keys(schema.uk[schema.tableName])[0];
        key = key.split('_').map(_.camelCase).join('_');
        p.print('unique: \'' + key + '\',');
      }

      p.print('},');

      p.print('createdBy: {');
      p.print('type: DataTypes.STRING(50),');
      p.print('allowNull: false,');
      p.print('},');

      p.print('updatedBy: {');
      p.print('type: DataTypes.STRING(50),');
      p.print('allowNull: false,');
      p.print('},');
    },
    // options
    standardOptions(p, schema) {
      if (extraUniqueKeys.length > 0) {
        p.print('indexes: [');
        extraUniqueKeys.map(uk => {
          const { ukColumns, key } = uk;
          p.print(`{ unique: '${key}', fields: [${ukColumns.map(col => `'${col}'`).join(', ')}] },`);
        });
        p.print('],');
      }
      // p.print('freezeTableName: true,');
      // p.print('createdAt: false,');
      // p.print('updatedAt: false');
    },
    // extend
    standardExtend(p, schema) {
      var relationshipCounter = 0;
      var block = p.createBlock();

      /**
       * Generate meta
       */
      // block.print(() => {
      //   p.print(`Model.meta = {`);
      //   p.print(`entityName: '${schema.tableName}',`);
      //   p.print(`moduleName: '${schema.moduleName}',`);
      //   if (schema.reference != null) {
      //     p.print(`reference: '${schema.reference}',`);
      //   }
      //   p.print(`interfaces: [`);
      //   p.tab();
      //   schema.interfaces.map(interface => {
      //     p.print(`'${interface.key}',`);
      //   })
      //   p.untab();
      //   p.print(`],`);
      //   p.print(`};`);
      //   p.print(`Model.prototype.getMeta = function () { return models['${schema.tableName}'].meta };`);

      //   if (schema.interfaces.has(I.HasEntity)) {
      //     p.print('Model.prototype.getEntity = function () {');
      //     p.print('return models[this.getDataValue(\'entityName\')]');
      //     p.tab();
      //     p.print('.findById(this.getDataValue(\'entityId\'));');
      //     p.untab();
      //     p.print('};');
      //   }
      // });

      /**
       * Generate relationship
       */
      schema.fks.map(function (fk, index) {
        var type = fk.type;
        var targetTableName = _.camelCase(fk.tableName);
        var foreignKey = _.camelCase(fk.columnName);
        var as = '';

        // associations
        if (type === 'hasMany' || type === 'hasOne') {
          var asWithoutPlural = foreignKey.trim().replace(/Id$/i, '');
          as = type === 'hasMany' ? inflect.pluralize(targetTableName) : targetTableName;
        }

        if (type === 'belongsTo') {
          // remove id from foreignKey name, require convention => TableName/RealName + Id/Key/Code,
          as = foreignKey.trim().replace(/Id$/i, '');
          as = as.replace(/Key$/i, '');
          as = as.replace(/Code$/i, '');
          as = as.replace(/Number$/i, '');
        }

        block.print(() => {
          p.print(`Model.${as} = Model.${type}(models.${targetTableName}, {`);
          p.print(`as: '${as}',`);
          p.print(`foreignKey: '${foreignKey}',`);

          // Refer: http://docs.sequelizejs.com/manual/tutorial/associations.html#foreign-keys
          // onDelete CASCADE is sequelize default when field is not allow null, so no need to print
          if (fk.onDelete != null && fk.onDelete !== 'CASCADE')
            p.print(`onDelete: '${fk.onDelete}',`);
          // onUpdate CASCADE is sequelize default, so no need to print
          if (fk.onUpdate != null && fk.onUpdate !== 'CASCADE')
            p.print(`onUpdate: '${fk.onUpdate}',`);
          p.print('});');
        });
      });

      if (schema.interfaces.has(I.HasComment))
        printEntityableAssociation('comment');

      if (schema.interfaces.has(I.HasAttachment))
        printEntityableAssociation('attachment');

      if (schema.interfaces.has(I.HasAddress))
        printEntityableAssociation('address');

      function printEntityableAssociation(targetTableName, as) {
        block.print(() => {
          if (as == null)
            as = inflect.pluralize(targetTableName);
          p.print(`Model.${as} = Model.hasMany(models.${targetTableName}, {`);
          p.print(`as: '${as}',`);
          p.print('foreignKey: \'entityId\',');
          p.print('constraints: false,');
          p.print(`scope: { entityName: '${_.camelCase(schema.tableName)}' },`);
          p.print('});');
        });
      }

      /**
       * Create relationship based on special column names
       * - attributeValue, contact, address, uom, currency
       */
      var columnNames = _.mapValues(schema.columns, c => c.columnName)
      function printSpecialBlock(as, targetTableName, columnName) {
        block.print(() => {
          p.print(`Model.${as} = Model.belongsTo(models.${targetTableName}, {`);
          p.print(`as: '${as}',`); // remove Id
          p.print(`foreignKey: '${columnName}',`);
          p.print('});');
        });
      }

      // // Handle *contactId
      // if (schema.tableName !== 'contact') {
      //   var columns = _.filter(columnNames, (c) => c.match(/contactId$/i));

      //   columns.map(function (columnName, index) {
      //     var targetTableName = 'contact';
      //     var as = columnName.trim().replace(/Id$/i, '');
      //     printSpecialBlock(as, targetTableName, columnName)
      //   });
      // }

      // Handle *addressId
      if (schema.tableName !== 'address') {
        var columns = _.filter(columnNames, (c) => c.match(/addressId$/i));

        columns.map(function (columnName, index) {
          var targetTableName = 'address';
          var as = columnName.trim().replace(/Id$/i, '');
          printSpecialBlock(as, targetTableName, columnName)
        });
      }

      // Handle *uom
      if (schema.tableName !== 'uom') {
        var columns = _.filter(columnNames, (c) => c.match(/uom$/i));

        columns.map(function (columnName, index) {
          var targetTableName = 'uom';
          var as = columnName.trim() + 'Data';
          printSpecialBlock(as, targetTableName, columnName)
        });
      }

      // Handle *currency
      if (schema.tableName !== 'currency') {
        var columns = _.filter(columnNames, (c) => c.match(/currency$/i));

        columns.map(function (columnName, index) {
          var targetTableName = 'currency';
          var as = columnName.trim() + 'Data';
          printSpecialBlock(as, targetTableName, columnName)
        });
      }
    },
  };
}
