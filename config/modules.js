var I = require('./interfaces');

module.exports = {
  accessControl: {
    permission: {
      interfaces: [],
    },
    permissionGroup: {
      interfaces: [],
    },
    permissionGroupItem: {
      interfaces: [],
    },
    role: {
      interfaces: [],
    },
    rolePermission: {
      interfaces: [],
    },
    rolePermissionGroup: {
      interfaces: [],
    },
    user: {
      interfaces: [],
    },
    userPermission: {
      interfaces: [],
    },
    userPermissionGroup: {
      interfaces: [],
    },
    userRole: {
      interfaces: [],
    },
  },
  system: {
    address: {
      interfaces: [],
    },
    attachment: {
      interfaces: [],
    },
    comment: {
      interfaces: [I.HasAttachment],
    },
    contact: {
      interfaces: [I.HasAddress],
    },
    list: {
      interfaces: [],
    },
    listItem: {
      interfaces: [],
    },
    notification: {
      interfaces: [],
    },
    setting: {
      interfaces: [],
    }
  },
  ticketing: {
    module: {
      dependencies: [
        'accessControl', 'system', 'business',
      ],
    },
    salesInvoice: {
      storeSync: { readBy: 'AllStore', writeBy: 'Master', nature: 'Transaction' },
      interfaces: [I.Document],
    },
    salesInvoiceItem: {
      storeSync: { readBy: 'AllStore', writeBy: 'Master', nature: 'Transaction' },
      interfaces: [I.ChildEntity],
    },
    ticket: {
      storeSync: { readBy: 'AllStore', writeBy: 'Master', nature: 'Transaction' },
      interfaces: [I.HasComment],
    }
  },
  crm: {
    businessDeal: {
      interfaces: [I.SoftDelete],
    },
    businessActivity: {
      interfaces: [I.HasComment, I.HasAttachment, I.SoftDelete],
    },
  },
  travel: {
    travelRecord: {
      interfaces: [],
    },
    claim: {
      interfaces: [],
    },
  },
  inventory: {
    product: {
      interfaces: [I.HasComment, I.HasAttachment],
    },
    item: {
      interfaces: [I.HasComment, I.HasAttachment],
    },
    location: {
      interfaces: [],
    }
  },
};
