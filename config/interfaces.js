var _ = require('lodash');

var interfaces = {
  // Behavioral
  HasProfilePicture: {
    name: 'Has Profile Picture',
    desc: 'Has a column to hold profile picture id',
  },
  HasPrimaryAddress: {
    name: 'Has Primary Address',
    desc: 'Has a column to hold primary address',
  },
  HasStatus: {
    name: 'Has Status',
    desc: 'Has a column to hold status',
  },
  Codeable: {
    name: 'Codeable',
    desc: 'Single natural key as PK',
  },
  NonUserEditable: {
    name: 'Non User Editable',
    desc: 'Cannot be editable by user',
  },
  NonDeletable: {
    name: 'Non Deletable',
    desc: 'Cannot be deleted once created',
  },
  Submittable: {
    name: 'Submittable',
    desc: 'Can have submit, cancel, amend lifecycle function',
  },
  Importable: {
    name: 'Importable',
    desc: 'Can have import entity function',
  },
  Exportable: {
    name: 'Exportable',
    desc: 'Can be exported',
  },
  SoftDelete: {
    name: 'SoftDelete',
    desc: 'Cannot be delete, but can be mark as delete',
  },
  Hierarchical: {
    name: 'Hierarchical',
    desc: 'Self referring hierarchical entity',
  },
  // Existential
  MappingEntity: {
    name: 'Mapping Entity',
    desc: 'Mapping table of two tables',
  },
  ChildEntity: {
    name: 'Child Entity',
    desc: 'Child entity of another table',
  },
  Document: {
    name: 'Document',
    desc: 'A document with line items',
  },
  BackgroundTask: {
    name: 'Background Task',
    desc: 'A type of background task',
  },
  // Relational
  HasEntity: {
    name: 'Has Entity',
    desc: 'Linked with another entity with entityName and entityId',
  },
  HasAddress: {
    name: 'Has Address',
    desc: 'Entity can have address on it',
  },
  HasComment: {
    name: 'Has Comment',
    desc: 'Entity can have comment on it',
  },
  HasAttachment: {
    name: 'Has Attachment',
    desc: 'Entity can have attachment on it',
  },
};
// add `key` to interface
_.keys(interfaces).map(i => {
  var interfaceName = _.camelCase(i);
  interfaces[i].key = interfaceName;
});
module.exports = interfaces;