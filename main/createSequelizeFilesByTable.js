var fs = require('fs');
var async = require('async');
var _ = require('lodash');

module.exports = function (schemas, fileGenarator, filedir, callback) {
  async.forEachSeries(
    _.values(schemas),
    function (schema, callback) {
      var subfolder = schema.moduleName || 'other';
      // var exactFiledir = schema.moduleName == null ? filedir : filedir + '/' + schema.moduleName;
      var filename = `${filedir}/${subfolder}/models/${schema.tableName}.js`;

      try {
        var buffer = fileGenarator(schema);
        if (filedir == null) {
          console.log(buffer);
          callback(null);
        }

        var mask = 0755;
        var err = mkdirSyncSafe(filedir + '/' + subfolder, mask);
        if (err && err.code != 'EEXIST')
          callback(err);

        err = mkdirSyncSafe(filedir + '/' + subfolder + '/models', mask);
        if (err && err.code != 'EEXIST')
          callback(err);
        
        var fd = fs.openSync(filename, 'wx', 0644);
        fs.writeSync(fd, buffer);
        fs.closeSync(fd);
        console.log(filename + ': created.');
        callback(null);
      } catch (err) {
        if (err.code == 'EEXIST') {
          console.log(filename + ': file already exists!');
          callback(null);
        }
        else {
          callback(err);
        }
      }
    },
    function (err) {
      callback(err);
    }
  );
};

function mkdirSyncSafe(dir, mask) {
  try {
    fs.mkdirSync(dir, mask);
  } catch (err) {
    return err;
  }
  return null;
}