var _ = require('lodash');

module.exports = function convertSchemas(schemas, resources, interfaces, callback) {
  var newSchemas = {};

  _.keys(schemas).map(table => {
    var tableName = _.camelCase(table);
    var schema = schemas[table];
    if (schema == null) return;

    newSchemas[tableName] = convertSchema(schema, resources, interfaces);
  });

  //console.log(newSchemas);

  callback(null, newSchemas);
}

function convertSchema(schema, resources, interfaces) {
  var tableName = _.camelCase(schema.table);

  var resource = resources[tableName];

  var newSchema = {
    tableName: tableName,
    columns: convertColumns(schema.columns, resource, interfaces),
    pk: schema.pk,
    uk: schema.uk,
    fks: convertFks(schema.fks),
    interfaces: [],
  };

  if (resource != null) {
    newSchema.interfaces = resource.interfaces;
    newSchema.moduleName = resource.moduleName;
    newSchema.entityName = resource.entityName;
    newSchema.resourceKey = resource.resourceKey;
    if (resource.reference)
      newSchema.reference = resource.reference;
  }
  newSchema.interfaces.has = (interface) => {
    return newSchema.interfaces.indexOf(interface) !== -1;
  }

  return newSchema;
}

function convertFks(fks) {
  return fks.map(convertFk)
}
function convertFk(fk) {
  if (fk.onDelete != null)
    fk.onDelete = convertRule(fk.onDelete);
  if (fk.onUpdate != null)
    fk.onUpdate = convertRule(fk.onUpdate);

  return fk;
}
function convertRule(rule) {
  switch (rule) {
    case 'noAction':
      return 'NO ACTION';
    case 'cascade':
      return 'CASCADE';
    case 'setNull':
      return 'SET NULL';
    case 'setDefault':
      return 'SET DEFAULT';
    default:
      return rule;
  }
}

function convertColumns(columns, resource, interfaces) {
  var newColumns = {};

  _.keys(columns).map(i => {
    var column = columns[i];
    var columnName = _.camelCase(column.colName)
    if (column == null) return;

    newColumns[columnName] = convertColumn(columnName, column, resource);
  });

  return newColumns;
}

function convertColumn(columnName, column, resource) {
  var colAttrs = column.columns;
  var newColumn = {
    columnName: columnName,
    tableName: _.camelCase(colAttrs['TABLE_NAME']),
    attributes: {
      tableCatelog: colAttrs['TABLE_CATALOG'],
      tableSchema: colAttrs['TABLE_SCHEMA'],
      isNullable: convertBoolean(colAttrs['IS_NULLABLE']),
      isIdentity: convertBoolean(colAttrs['IS_IDENTITY']),
      isJSON: checkIsJSON(colAttrs),
      defaultValue: convertDefaultValue(colAttrs),
      dataType: convertDataType(colAttrs),
      characterMaxLength: colAttrs['CHARACTER_MAXIMUM_LENGTH'],
      characterOctetLength: colAttrs['CHARACTER_OCTET_LENGTH'],
      numericPrecision: colAttrs['NUMERIC_PRECISION'],
      numericPrecisionRadix: colAttrs['NUMERIC_PRECISION_RADIX'],
      numericScale: colAttrs['NUMERIC_SCALE'],
    },
    erpAttributes: {
      // log where this columns is created
      // and reason. E.G. auto generated
      createdSource: '',
    }
  };

  //console.log(newColumn);
  return newColumn;
}

function checkIsJSON(colAttrs) {
  return colAttrs['DATA_TYPE'] === 'text';
}

function convertDataType(colAttrs) {
  switch (colAttrs['DATA_TYPE']) {
    case 'nvarchar':
    case 'nchar':
    case 'varchar':
    case 'char': {
      var length = colAttrs['CHARACTER_MAXIMUM_LENGTH'];
      if (length === -1) length = 4000;
      return 'DataTypes.STRING(' + length + ')';
    }

    case 'uniqueidentifier':
      return 'DataTypes.UUID';

    case 'bigint':
      return 'DataTypes.BIGINT';
    case 'smallint':
    case 'tinyint':
    case 'int':
      return 'DataTypes.INTEGER';
    case 'float':
      return 'DataTypes.FLOAT';
    case 'datetime':
      return 'DataTypes.DATE';
    case 'text':
    case 'ntext':
      return 'DataTypes.TEXT';
    case 'varbinary':
    //	return 'DataTypes.STRING.BINARY';
    case 'image':
      return 'DataTypes.BLOB';
    case 'bit':
      return 'DataTypes.BOOLEAN';
    case 'time':
      return 'DataTypes.TIME';
    case 'datetime':
      return 'DataTypes.DATE';
    case 'date':
      return 'DataTypes.DATEONLY';
    case 'money':
    case 'decimal':
      return 'DataTypes.FLOAT';
    //return 'DataTypes.DECIMAL(' + column['NUMERIC_PRECISION'] + ', ' + column['NUMERIC_SCALE'] + ')';
    default:
      throw new Error('Oops! please add proper datatype for "' + colAttrs['DATA_TYPE'] + ' in ' + __filename);
  }
};

function convertDefaultValue(colAttrs) {
  var defaultValue = colAttrs['COLUMN_DEFAULT'];
  if (defaultValue == null) return null;

  var result = defaultValue.trim();

  // strip first pair of ()
  if (result.startsWith('(')) {
    result = result.substring(1, result.length - 1);
  }

  // strip again for integer
  if (result.startsWith('(')) {
    result = result.substring(1, result.length - 1);
  }

  // strip string
  if (result.startsWith('N\'')) {
    result = result.substring(1, result.length);
  }

  // convert boolean
  if (colAttrs['DATA_TYPE'] === 'bit') {
    if (result === '0') result = false;
    if (result === '1') result = true;
  }

  //console.log(result);
  return result;
};

function convertBoolean(attr) {
  switch (attr) {
    case "YES":
      return true;
    case "NO":
      return false;
    default:
      return attr;
  }
};