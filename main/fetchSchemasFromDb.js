var tedious = require('tedious');
var fs = require('fs');
var async = require('async');
var _ = require('lodash');

var Connection = tedious.Connection;
var Request = tedious.Request;

module.exports = function (cfg, dbname, filedir, callback) {
  var config = JSON.parse(JSON.stringify(cfg));

  config.options = config.options || {};

  config.options.rowCollectionOnRequestCompletion = false;
  config.options.rowCollectionOnDone = false;

  var connection = new Connection(config);

  connection.on('connect', function (err) {
    if (err) {
      console.log(err);
      return;
    }

    async.waterfall([
      function (callback) {
        var request = new Request(
          "USE " + dbname,
          function (err, rowCount) {
            if (err) {
              callback(err);
            } else {
              callback(null);
            }
          }
        );

        connection.execSql(request);
      },
      // TABLES
      function (callback) {
        var tables = [];
        var excludeTables = config.excludeTables;
        var excludeTableString = excludeTables.map(t => `'${t}',`).join('');
        console.log(excludeTableString);
        var request = new Request(
          `SELECT TABLE_NAME FROM information_schema.TABLES WHERE TABLE_TYPE = 'BASE TABLE' AND TABLE_NAME NOT IN (${excludeTableString}'query', 'sysdiagrams')`,
          //"SELECT DB_NAME()",
          // "SELECT name from Sys.Databases",
          function (err, rowCount, rows) {
            if (err) {
              callback(err);
            } else {
              callback(null, tables);
            }
          }
        );

        request.on('row', function (columns) {
          //console.log(columns);
          columns.forEach(function (column) {
            tables.push(_.camelCase(column.value));
            // process.stdout.write("[" + column.metadata.colName + ": " + column.value + "]");
          });
          // process.stdout.write("\n");
        });

        connection.execSql(request);
      },
      // TABLES' COLUMN INFO
      function (tables, callback) {
        var TYPES = tedious.TYPES;
        var schemas = {};

        async.forEachSeries(
          tables,
          function (tableName, callback) {
            var schema = {
              table: tableName,
              columns: [],
              props: {},
              pk: {},
              uk: {},
              fks: [],
              // belongsTo: {},
              // hasMany: {},
              // hasOne: {},
            };

            var request = new Request(
              `
							SELECT c.*, s.is_identity IS_IDENTITY FROM
							information_schema.COLUMNS c,
							sys.columns s
							WHERE (c.TABLE_NAME = @name AND TABLE_CATALOG = @catalog )
							AND (s.object_id = OBJECT_ID(c.TABLE_NAME) AND s.name = c.COLUMN_NAME )
							ORDER BY ORDINAL_POSITION
						`,
              function (err, rowCount) {
                if (err) {
                } else {
                  schemas[tableName] = schema;
                  callback(null);
                }
              }
            );

            request.on('row', function (columns) {
              //console.log(columns);
              var cols = {
                colName: undefined,
                columns: {}
              };

              columns.forEach(function (column) {
                if (column.metadata.colName === 'COLUMN_NAME') {
                  cols.colName = column.value;
                }

                cols.columns[column.metadata.colName] = column.value;
              });

              schema.columns.push(cols);
            });

            request.addParameter('name', TYPES.VarChar, tableName);
            request.addParameter('catalog', TYPES.VarChar, dbname);

            connection.execSql(request);
          },
          function (err) {
            if (err) {
              callback(err);
            } else {
              callback(null, schemas);
            }
          }
        );
      },
      // TABLE PROPERTY
      function (schemas, callback) {
        var TYPES = tedious.TYPES;

        var request = new Request(
          `
					SELECT T.NAME TABLE_NAME, P.NAME PROPERTY_NAME, P.VALUE PROPERTY_VALUE
					FROM SYS.EXTENDED_PROPERTIES P
					INNER JOIN SYS.TABLES T ON P.MAJOR_ID = T.OBJECT_ID
					WHERE CLASS = 1 AND P.minor_id = 0
					`,
          function (err) {
            if (err) {
              callback(err);
            } else {
              callback(null, schemas);
            }
          }
        );

        request.on('row', function (columns) {
          //console.log(columns);
          var tableName = _.camelCase(columns[0].value);
          var propertyName = _.camelCase(columns[1].value);
          var propertyValue = columns[2].value;
          var schema = schemas[tableName];

          if (schema == null) return;
          schema.props[propertyName] = propertyValue;
        });

        request.addParameter('catalog', TYPES.VarChar, dbname);

        connection.execSql(request);
      },
      // PRIMARY KEY
      function (schemas, callback) {
        var TYPES = tedious.TYPES;

        var request = new Request(
          `
					SELECT TABLE_NAME, COLUMN_NAME FROM information_schema.KEY_COLUMN_USAGE
					WHERE TABLE_CATALOG= @catalog AND
					CONSTRAINT_NAME LIKE 'PK_%' ORDER BY ORDINAL_POSITION
					`,
          function (err) {
            if (err) {
              callback(err);
            } else {
              callback(null, schemas);
            }
          }
        );

        request.on('row', function (columns) {
          //console.log(columns);
          var tableName = _.camelCase(columns[0].value);
          var columnName = _.camelCase(columns[1].value);
          var schema = schemas[tableName];

          if (schema == null) return;
          schema.pk[columnName] = _.keys(schema.pk).length;
        });

        request.addParameter('catalog', TYPES.VarChar, dbname);

        connection.execSql(request);
      },
      // UNIQUE KEY
      function (schemas, callback) {
        var TYPES = tedious.TYPES;

        var request = new Request(
          `
						SELECT CC.TABLE_NAME, TC.CONSTRAINT_NAME, CC.COLUMN_NAME
						FROM information_schema.table_constraints TC
						INNER JOIN information_schema.constraint_column_usage CC
						ON TC.CONSTRAINT_NAME = CC.CONSTRAINT_NAME
						WHERE TC.CONSTRAINT_TYPE = 'Unique'
						AND TC.TABLE_CATALOG=@catalog
						ORDER BY CC.TABLE_NAME, CC.COLUMN_NAME
					`,
          function (err) {
            callback(null, schemas);
          }
        );

        request.on('row', function (columns) {
          //console.log(columns);
          var tableName = _.camelCase(columns[0].value);
          var constraintName = columns[1].value;
          var columnName = _.camelCase(columns[2].value);
          var schema = schemas[tableName];

          if (schema == null) return;
          schema.uk[tableName] = schema.uk[tableName] || {};
          schema.uk[tableName][constraintName] = schema.uk[tableName][constraintName] || [];
          schema.uk[tableName][constraintName].push(columnName);
        });

        request.addParameter('catalog', TYPES.VarChar, dbname);

        connection.execSql(request);
      },
      // FOREIGN KEY
      function (schemas, callback) {
        var TYPES = tedious.TYPES;

        var request = new Request(
          `
						SELECT
							ccu.table_name AS SourceTable
							,ccu.constraint_name AS SourceConstraint
							,ccu.column_name AS SourceColumn
							,kcu.table_name AS TargetTable
							,kcu.column_name AS TargetColumn
							,rc.UPDATE_RULE
							,rc.DELETE_RULE
						FROM INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE ccu
								INNER JOIN INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc
										ON ccu.CONSTRAINT_NAME = rc.CONSTRAINT_NAME
								INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE kcu
										ON kcu.CONSTRAINT_NAME = rc.UNIQUE_CONSTRAINT_NAME
					`,
          function (err) {
            callback(null, schemas);
          }
        );

        request.on('row', function (columns) {
          //console.log(columns);
          var sourceTableName = _.camelCase(columns[0].value);
          var constraintName = columns[1].value;
          var sourceColumnName = _.camelCase(columns[2].value);
          var targetTableName = _.camelCase(columns[3].value);
          var targetColumnName = _.camelCase(columns[4].value);
          var onUpdate = _.camelCase(columns[5].value);
          var onDelete = _.camelCase(columns[6].value);
          var sourceSchema = schemas[sourceTableName];
          var targetSchema = schemas[targetTableName];

          if (sourceSchema == null || targetSchema == null) return;

          // if source only single primary key and that key is the foreign key for targetColumnName
          // then it is a has one relationship
          if (_.keys(sourceSchema.pk)[0] === sourceColumnName && _.keys(sourceSchema.pk).length === 1) {
            targetSchema.fks.push({
              tableName: sourceTableName,
              columnName: sourceColumnName,
              onUpdate, onDelete,
              type: 'hasOne'
            });

            sourceSchema.fks.push({
              tableName: targetTableName,
              columnName: sourceColumnName,
              type: 'belongsTo'
            });
          } else {
            targetSchema.fks.push({
              tableName: sourceTableName,
              columnName: sourceColumnName,
              onUpdate, onDelete,
              type: 'hasMany'
            });

            sourceSchema.fks.push({
              tableName: targetTableName,
              columnName: sourceColumnName,
              type: 'belongsTo'
            });
          }
        });

        request.addParameter('catalog', TYPES.VarChar, dbname);

        connection.execSql(request);
      },
    ], function (err, data) {
      connection.close();
      callback(err, data);
    });
  });
}