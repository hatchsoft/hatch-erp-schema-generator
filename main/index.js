// var db2sequelize = require('./lib');
var fs = require('fs');
var async = require('async');
var fetchSchemasFromDb = require('./fetchSchemasFromDb');
var createSequelizeFilesByTable = require('./createSequelizeFilesByTable');
var createModuleDefinitionFiles = require('./createModuleDefinitionFiles');
var convertSchemas = require('./convertSchemas');
var generate = require('../generate');

var _ = require('lodash');

module.exports = function (cfg, resources, interfaces, filedir, callback) {
	if (typeof filedir == 'function') {
		callback = filedir;
		filedir = null;
	}
	var importSchema = false;
	var replaceFile = true;

	async.waterfall([
		function (callback) {
			createFolderIfNotCreated(replaceFile, filedir, callback);
		},
		function (callback) {
			if (!importSchema) {
				fetchSchemasFromDb(cfg, cfg.database, filedir, callback);
			} else {
				importSchemasFromJson('sampleSchema.json', './config/', callback);
			}
		},
		function (schemas, callback) {
			exportRawSchema(schemas, 'sampleSchema.json', './config/', callback);
		},
		function (schemas, callback) {
			// process schema object
			// add on table level and module level details here.
			convertSchemas(schemas, resources, interfaces, callback);
		},
		function (schemas, callback) {
			createSequelizeFilesByTable(schemas, generate, filedir, callback);
		},
		function (callback) {
			createModuleDefinitionFiles(resources, interfaces, filedir, callback);
		},
	], function (err) {
		callback(err);
	});
};

function createFolderIfNotCreated(replaceFile, filedir, callback) {
	if (filedir == null) {
		callback(null);
	} else {
		var mask = 0755;

		fs.mkdir(filedir, mask, function (err) {
			if (err) {
				if (replaceFile && err.code == 'EEXIST') {
					rmdirFileContent(filedir);
				}
				callback(err.code == 'EEXIST' ? null : err);
			} else {
				callback(null);
			}
		});
	}

	function rmdirFileContent(dirPath) {
		try { var files = fs.readdirSync(dirPath); }
		catch (e) { return; }
		if (files.length > 0)
			for (var i = 0; i < files.length; i++) {
				var filePath = dirPath + '/' + files[i];
				if (fs.statSync(filePath).isFile())
					fs.unlinkSync(filePath);
				else
					rmdirFileContent(filePath);
			}
		// do not remove folder
		//fs.rmdirSync(dirPath);
	};
}

function exportRawSchema(schemas, filename, filedir, callback) {
	// save schema JSON file
	var json = JSON.stringify(schemas, null, 2);
	if (filedir != null) {
		fs.writeFile(filedir + '/' + filename, json, 'utf8', function () {
			callback(null, schemas);
		});
	} else {
		callback(null, schemas);
	}
}

function importSchemasFromJson(filename, filedir, callback) {
	var schemas = JSON.parse(fs.readFileSync(filedir + '/' + filename, 'utf8'));
	callback(null, schemas);
}