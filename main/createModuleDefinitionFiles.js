var fs = require('fs');
var _ = require('lodash');
var PrinterFactory = require('../generate/PrinterFactory');

module.exports = function(resources, interfaces, filedir, callback) {

  var definition = {};

  // sort by modules first to output according to files
  _.map(resources, function (item) {
      if (definition[item.moduleName] == null)
        definition[item.moduleName] = [];

      definition[item.moduleName].push(item);
  });

  _.keys(definition).map(function (moduleName) {
    var dir = filedir + '/' + moduleName;
    try {
      fs.mkdirSync(dir);
    } catch (err) {
      if (err && err.code != 'EEXIST') {
        console.log('fail to create "' + moduleName + '" definition file directory');
        return;
      }
    }

    try {
      var filename = dir + '/index.json';
      var fd = fs.openSync(filename, 'w');
      var buffer = generator(definition[moduleName]);
      fs.writeSync(fd, buffer);
      fs.closeSync(fd);
      console.log('created definition file for: ', filename);
    } catch (err) {
      if (err.code == 'EEXIST')
        console.log(moduleName + ': definition file already exists!');
      else
        console.log(err);
    }
  });

  callback(null);
};

function generator(entities) {
  var p = PrinterFactory({ indent: 0, tabsize: 2 });

  // p.print('module.exports = {');  
  p.print('{');

  _.map(entities, function (item) {
    p.print('"' + item.entityName + '": {');
    var res = JSON.stringify(item, null, 2).split('\n');
    res.shift(); // remove 1st bracket
    res.splice(-1, 1); // remove the last bracket
    res.map(function(content) {

      content = content.trim();
      p.print(content);
    });
    p.print('},');
  });

  p.print('}');
  return p.printAll();
}
