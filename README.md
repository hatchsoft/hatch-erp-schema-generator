# hatch-lite-schema-generator
Reverse engineer an existing mssql database into a Sequelize database model.
A fork from hatch-erp-schema-generator with different format

Remember to run `npm install` for the first time setup.

Type `node index` to start.

## Folder structure and flow
- Configuration files are in `config` folder.
  - `config.js`: configure database details here
  - `interfaces.js`: define entity interfaces here
  - `modules.js`: define modules related meta info here
  - `sampleSchema.json`: schema object JSON dump to replace MS SQL instance if not installed
- `main` folder contains files that describe the full flow.
- Current flow is as below
  - Create target build folder if not created
  - Generate schemas either from a JSON file or from MsSQL database
  - [Optional] export schema to a JSON file for future use
  - Generate Sequelize file by table based on the generated schemas above
- `generate` folder contains all logic to generate Sequelize file for a table from a table schema
  - Main file structure are defined in GeneratorFactory
  - GeneratorFactory uses a Printer to generate a file
  - We can split complex logic into smaller parts by creating function with following signature: 
    ```javascript
    var generator(p, schema) => {
      p.print('a new line of code');
      p.print(`print something from schema ${schema.tableName}`);
      p.tab('if I want to indent a layer');
      p.print('more codes');
      p.untab('remember to unindent');
      print('auto tab if ends with {');
      print('}, // auto untab if start with ');
    }
    ```
  - This way, complex generation logic can be split into different parts and be composed in the GeneratorFactory code
- Generated Sequelize file's format will be accepted and processed by the `sequelizeImportModels()` function in `databaseBuilder` in `hatch-framework` project.


## Credit
This project was initially cloned and then rewritten by referring to the project below:
Reference: https://github.com/blue-saber/mssql-2-sequelize-bo

## Notes
- The generated file is vastly different from the project above
- Many logic are hard coded, need to be generalized before becoming a generic tool

## Help
- If you receive Connection Error during connection, most likely it is because the TCP/IP of the SQL Server is disabled. Set it up follow this link:
http://support.webecs.com/kb/a868/how-do-i-configure-sql-server-express-to-allow-remote-tcp-ip-connections-on-port-1433.aspx